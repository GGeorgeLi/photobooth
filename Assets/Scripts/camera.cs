﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class camera : MonoBehaviour
{
    private bool camAvailable;
    private WebCamTexture Cam;
    private Texture defaultBackground;

    public RawImage background;
    public AspectRatioFitter fit;

    private void Start()
    {
        defaultBackground = background.texture;
        WebCamDevice[] devices = WebCamTexture.devices;

        if(devices.Length == 0)
        {
            Debug.Log("No camera detected");
            camAvailable = false;
            return;
        }

        for (int i = 0; i < devices.Length; i++)
        {
 
           Cam = new WebCamTexture(devices[i].name, Screen.width, Screen.height);
        }

        if (Cam == null)
        {
            Debug.Log("Unable to find camera");
            return;
        }

        Cam.Play();
        background.texture = Cam;

        camAvailable = true;

    }

    private void Update()
    {
        if (!camAvailable)
            return;

        float ratio = (float)Cam.width / (float)Cam.height;
        fit.aspectRatio = ratio;

        float scaleY = Cam.videoVerticallyMirrored ? -1f : 1f;
        background.rectTransform.localScale = new Vector3(1f, scaleY, 1f);

        int orient = -Cam.videoRotationAngle;
        background.rectTransform.localEulerAngles = new Vector3(0, 0, orient);

    }




    void OnGUI()
    {
        if (GUI.Button(new Rect(10, 10, 150, 100), "Click"))
        {
            TakeSnapshot();
            Debug.Log("Photo has been taken");
        }
           


    }

    // For saving to the _savepath
    private string _SavePath = "C:/Users/Admin/FutureLabs/Unity Project/Caltex/PhotoBooth/Assets/Captures/"; 
    int _CaptureCounter = 0;

    void TakeSnapshot()
    {
        Texture2D snap = new Texture2D(Cam.width, Cam.height);
        snap.SetPixels(Cam.GetPixels());
        snap.Apply();

        System.IO.File.WriteAllBytes(_SavePath + _CaptureCounter.ToString() + ".png", snap.EncodeToPNG());
        ++_CaptureCounter;
    }
}

